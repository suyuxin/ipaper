__author__ = 'Yuxin Su'
from database import Database

path = '../static/publications.txt'

if __name__ == '__main__':
    database = Database()
    authors = None
    new_paper_id = 0
    for line in open(path, 'r'):
        if line.startswith('#index'):
            #save old
            if authors:
                database.update_authors(new_paper_id, authors)

            new_paper_id = int(line[6:])
            if new_paper_id % 500 == 0:
                print new_paper_id
            citation_list = None
        if line.startswith('#@'):
            try:
                authors = line[2:].strip(" \r\n")
            except ValueError, e:
                continue
    database.update_authors(None, None)