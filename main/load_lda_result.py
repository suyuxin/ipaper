__author__ = 'yxsu'
from database import Database
doc_path = "../static/doc.1_of_2"

database = Database()



for line in open(doc_path, 'r'):
    data = [int(item) for item in line.split('\t')]
    doc_id = str(data[0])
    topic_id = None
    for i in range(1, len(data)):
        if data[i] == 1:
            topic_id = i - 1
    database.save_doc_topics(doc_id, topic_id)
    print doc_id
