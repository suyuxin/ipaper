__author__ = 'Yuxin Su'
from database import Database

path = '../static/outputacm.txt'

if __name__ == '__main__':
    database = Database()
    citation_list = list()
    new_paper_id = 0
    for line in open(path, 'r'):
        if line.startswith('#index'):
            #save old
            if len(citation_list) > 0:

                database.add_citation_via_transfer(new_paper_id, citation_list)

            new_paper_id = int(line[6:])
            if new_paper_id % 500 == 0:
                print new_paper_id
            citation_list = list()
        if line.startswith('#%'):
            try:
                citation_list.append(int(line[2:]))
            except ValueError, e:
                continue