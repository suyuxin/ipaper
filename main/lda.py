__author__ = 'yxsu'

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF
from database import Database
from time import time

if __name__ == '__main__':
    database = Database()
    print "Load dictionary..."
    dictionary = database.get_dictionary()
    with open("../static/dictionary.txt", 'w') as f:
        for word in dictionary:
            f.write(word[0] + '\n')

    print "Load raw data..."
    tokens = database.get_all_tokens()
    with open("../static/tokens.txt", 'w') as f:
        for token in tokens:
            f.write("%d  %d  %d\n" % token)
