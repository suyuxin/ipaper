__author__ = 'Yuxin Su'
import MySQLdb as mdb
import random

class Paper:
    title = ''
    id = 1
    year = 2000
    publication = ''
    abstract = ''
    author = ""

    def __init__(self):
        pass


class Database:
    insert_buffer = list()
    transfer_buffer = list()

    #interface with paper result
    PAPER_ID = 0
    PAPER_TITLE = 1
    PAPER_PUBLICATION = 2
    PAPER_YEAR = 3
    PAPER_ABSTRACT = 4
    PAPER_PAGE_RANK = 5
    PAPER_AUTHOR = 6

    def __init__(self):
        self.connection = mdb.connect('10.10.6.51', 'root', 'curi504', 'ipaper')
        self.cursor = self.connection.cursor()

    def insertPaper(self, paper):
        value = (paper.id, mdb.escape_string(paper.title), mdb.escape_string(paper.publication), paper.year,
                 mdb.escape_string(paper.abstract), mdb.escape_string(paper.author))
        self.insert(value)

    def insert(self, command):
        self.insert_buffer.append(command)
        if len(self.insert_buffer) > 500:
            self.cursor.executemany("insert into ipaper.paper(id,title,publication,year,abstract,authors) values (%s,%s,%s,%s,%s,%s)", self.insert_buffer)
            self.commit()

    def commit(self):
        self.connection.commit()
        self.insert_buffer = []

    def get_max_id(self):
        self.cursor.execute("select id from ipaper.paper order by id desc limit 0, 1")
        result = self.cursor.fetchone()
        if result:
            return result[0]
        else:
            return 1

    def add_citation(self, new_paper, reference_list):
        values = [(new_paper, ref) for ref in reference_list]
        self.cursor.executemany("insert into ipaper.citation(paper_id, reference_id) values (%s, %s)", values)
        self.connection.commit()

    def add_citation_via_transfer(self, new_paper_id, reference_list):
        #find normal id
        self.cursor.execute("select normal_id from ipaper.id_transfer where temp_id = %s", (new_paper_id))
        normal_id = self.cursor.fetchone()[0]
        values = list()
        for ref in reference_list:
            #find normal id
            self.cursor.execute("select normal_id from ipaper.id_transfer where temp_id = %s", (ref))
            result = self.cursor.fetchone()
            if result:
                values.append((normal_id, result[0]))
        self.cursor.executemany("insert into ipaper.citation(paper_id, reference_id) values (%s, %s)", values)
        self.connection.commit()


    def get_all_paper_id_and_title(self):
        self.cursor.execute("select id, title from ipaper.paper")
        return self.cursor.fetchall()

    def get_paperIDs_with_valid_citation(self):
        self.cursor.execute("select paper_id from ipaper.citation group by paper_id")
        return [id[0] for id in self.cursor.fetchall()]

    def get_citation_of_paper(self, paper_id):
        self.cursor.execute("select reference_id from ipaper.citation where paper_id = "+str(paper_id))
        return [id[0] for id in self.cursor.fetchall()]

    def get_all_citations(self):
        self.cursor.execute("select paper_id, reference_id from ipaper.citation")
        return self.cursor.fetchall()

    def update_paper_page_rank(self, paper_id, value):
        self.cursor.execute("update ipaper.paper set page_rank = %s where id = %s", (value, paper_id))

    def get_all_title_and_abstract(self):
        self.cursor.execute("select id, title, abstract from ipaper.paper where title != ''")
        return self.cursor.fetchall()

    def update_bag_words(self, vocabulary):
        values = list()
        for word, index in vocabulary.iteritems():
            values.append((index, mdb.escape_string(word)))
        #save
        self.cursor.executemany("insert into ipaper.bag_of_word values(%s, %s)", values)
        self.commit()

    def save_posting_list(self, doc_id, word_id, freq):
        if doc_id:
            self.insert_buffer.append((doc_id, word_id, freq))
            if len(self.insert_buffer) < 10000:
                return
        #save
        self.cursor.executemany("insert into ipaper.posting_list_title(paper_id, word_id, term_freq) values(%s, %s, %s)", self.insert_buffer)
        self.commit()

    def save_doc_topics(self, doc_id, topic_id):
        if doc_id:
            self.insert_buffer.append((doc_id, topic_id))
            if len(self.insert_buffer) < 10000:
                return
        #save
        self.cursor.executemany("insert into ipaper.doc_topic(doc_id,topic_id) values(%s, %s)", self.insert_buffer)
        self.commit()

    def update_authors(self, doc_id, authors):
        if doc_id:
            self.insert_buffer.append((authors, doc_id))
            if len(self.insert_buffer) < 10000:
                return
        #save
        self.cursor.executemany("update ipaper.paper set authors = %s where id = %s", self.insert_buffer)
        self.commit()

    def get_word_id(self, word):
        self.cursor.execute("select word_id from ipaper.bag_of_word where word = %s", (word))
        result = self.cursor.fetchone()
        if result:
            return result[0]
        else:
            return None

    def get_doc_set_contain_word(self, word_id):
        self.cursor.execute("select paper_id from ipaper.posting_list_title where word_id = " + str(word_id))
        result = set()
        for item in self.cursor.fetchall():
            result.add(item[0])
        return result

    def get_paper_info(self, paper_id):
        self.cursor.execute("select * from ipaper.paper where id = " + str(paper_id))
        return self.cursor.fetchone()

    def get_sorted_doc_info_list(self, doc_id_set, limit=200):
        table_name = 'ipaper.query_' + str(random.randint(0, 1000))
        self.cursor.execute("create table "+table_name+" (id int not null, primary key(id))")
        self.cursor.executemany("insert into "+table_name +" values(%s)", doc_id_set)
        self.connection.commit()
        #search
        self.cursor.execute("select id,title,publication,year,page_rank,authors from ipaper.paper where id in (select id from "+table_name+") order by page_rank desc limit 0, %s", (limit))
        result = ''
        for record in self.cursor.fetchall():
            result += '(' + str(record[0])
            for i in range(1, len(record)):
                result += "^^ " + str(record[i])
            result += ')$'
        self.cursor.execute("drop table " + table_name)
        self.connection.commit()
        return result

    def get_doc_list_with_same_topic(self, doc_id):
        self.cursor.execute("select doc_id from ipaper.doc_topic where topic_id in (select topic_id from ipaper.doc_topic where doc_id = %s)", (doc_id))
        return [id[0] for id in self.cursor.fetchall()]

    def get_doc_list_prefered_by_user(self, user_id):
        if user_id == '354710054388646' or user_id == 354710054388646:
            self.cursor.execute("select doc_id from ipaper.temp_paper_list")
            return [id[0] for id in self.cursor.fetchall()]
        try:
            self.cursor.execute("select doc_id from ipaper.doc_topic where topic_id in (select topic_id from ipaper.doc_topic where doc_id in (select paper_id from ipaper.user_model where user_id = %s and pref_degree > 1.0))", (user_id))
            return [id[0] for id in self.cursor.fetchall()]
        except Exception, e:
            return None

    def get_sorted_paper_list(self, doc_id_set):
        result = []
        for id in doc_id_set:
            self.cursor.execute("select * from ipaper.paper where id = " + str(id))
            result.append(self.cursor.fetchone())
        result.sort(key=lambda info : info[self.PAPER_PAGE_RANK], reverse=True) #sorting via page_rank
        #create paper list
        paper_list = list()
        for item in result:
            paper = Paper()
            paper.title = item[self.PAPER_TITLE]
            paper.publication = item[self.PAPER_PUBLICATION]
            paper.abstract = item[self.PAPER_ABSTRACT]
            paper.year = item[self.PAPER_YEAR]
            paper_list.append(paper)
        return paper_list

    def get_abstract(self, paper_id):
        self.cursor.execute("select abstract from ipaper.paper where id = %s", (paper_id))
        return self.cursor.fetchone()[0]


    def update_user_model_record(self, user_id, paper_id, preference_degree):
        self.cursor.execute("select pref_degree from ipaper.user_model where user_id=%s and paper_id=%s", (user_id, paper_id))
        if self.cursor.fetchone():
            self.cursor.execute("update ipaper.user_model set pref_degree = %s where user_id = %s and paper_id = %s", (preference_degree, user_id, paper_id))
            self.commit()
        else:
            self.cursor.execute("insert into ipaper.user_model(user_id, paper_id, pref_degree) values (%s,%s,%s)", (user_id, paper_id, preference_degree))
            self.commit()

    def insert_paper_from_new_dataset(self, paper, new_id):
        if paper:
            #save id transfer
            self.transfer_buffer.append((paper.id, new_id))
            #save into database
            paper.id = new_id
            self.insertPaper(paper)
            if(len(self.transfer_buffer) < 1000):
                return new_id
        #save
        self.cursor.executemany("insert into ipaper.id_transfer values(%s,%s)", self.transfer_buffer)
        self.connection.commit()
        self.transfer_buffer = []

    def get_all_tokens(self):
        self.cursor.execute("select paper_id, word_id, term_freq from ipaper.posting_list_title")
        return self.cursor.fetchall()

    def get_dictionary(self):
        self.cursor.execute("select word from ipaper.bag_of_word order by word_id")
        return self.cursor.fetchall()