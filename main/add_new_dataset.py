__author__ = 'yxsu'
from database import *

path = "../static/outputacm.txt"



if __name__ == '__main__':
    database = Database()
    current_paper = None
    min_valid_id = database.get_max_id() + 1
    for line in open(path, 'r'):
        content = line[2:].strip(' \r\n')
        if line.startswith('#*'):
            if current_paper:
                #save old paper
                if min_valid_id == database.insert_paper_from_new_dataset(current_paper, min_valid_id):
                    min_valid_id += 1
            current_paper = Paper()
            current_paper.title = content
        elif line.startswith('#t'):
            current_paper.year = int(content)
        elif line.startswith('#c'):
            current_paper.publication = content
        elif line.startswith('#i'):
            current_paper.id = int(content[4:])
            if current_paper.id % 500 == 0:
                print current_paper.id
        elif line.startswith('#!'):
            current_paper.abstract = content

    database.insert_paper_from_new_dataset(None, None)