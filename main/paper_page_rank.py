__author__ = 'Yuxin Su'
from database import Database
import networkx as nx

if __name__ == '__main__':
    database = Database()
    print "Loading network..."
    all_citations = database.get_all_citations()
    print "Creating network..."
    G = nx.Graph()
    G.add_edges_from(all_citations)
    print "Number of Nodes : " + str(G.number_of_nodes())
    print "Number of Edges : " + str(G.number_of_edges())
    #run pagerank
    print "Run PageRank..."
    rank = nx.pagerank(G)
    print "Save results of PageRank"
    for id, value in rank.iteritems():
        print str(id) + ' : ' + str(value)
        database.update_paper_page_rank(id, value)
    database.commit()