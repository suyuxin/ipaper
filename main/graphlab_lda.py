__author__ = 'yxsu'

import sys
import os
import subprocess

n_topics = 50
time = 180 #time in second


graphlab_path = '~/workspace/graphlab/release/toolkits/topic_modeling/'
data_path = os.path.join(sys.path[0], "../static/")
output_directory = os.path.join(data_path, 'lda_result')
if not os.path.exists(output_directory):
    os.mkdir(output_directory)

arguments = " --corpus " + data_path + 'tokens.txt'
arguments += ' --dictionary ' + data_path + 'dictionary.txt'
arguments += ' --word_dir ' + output_directory + '/word_counts'
arguments += ' --doc_dir ' + output_directory + '/doc_counts'
arguments += ' --burnin=' + str(time)

#run
proc = subprocess.Popen(['cd ' + graphlab_path + ';./cgs_lda ' + arguments], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
(out, error) = proc.communicate()
print out