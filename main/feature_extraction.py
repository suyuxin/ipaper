__author__ = 'yxsu'


from sklearn.feature_extraction.text import CountVectorizer
from database import Database

if __name__ == '__main__':
    database = Database()
    print "Load raw data..."
    all_titles = database.get_all_title_and_abstract()

    doc_id_list = list()
    doc_list = list()

    for id, title, abstract in all_titles:
        doc_id_list.append(id)
        doc_list.append(title + " " + abstract)
    print "Begin to compute posting list..."
    #compute posting list
    count_vector = CountVectorizer(stop_words='english', token_pattern='[a-zAz][a-zAz]+')
    posting_matrix = count_vector.fit_transform(doc_list)
    print "Begin to save result..."
    #save bag of word
    database.update_bag_words(count_vector.vocabulary_)
    #save posting list
    docs, words = posting_matrix.nonzero()
    freq = posting_matrix.data
    size = len(docs)
    for i in range(size):
        database.save_posting_list(doc_id_list[docs[i]], words[i], freq[i])
        if i % 1000 == 0:
            print str(i) + '/' + str(size)
    database.save_posting_list(None, None, None)
