# -*- coding: utf-8 -*-
__author__ = 'Yuxin Su'

from database import Database
from database import Paper


path = '../static/publications.txt'

if __name__ == '__main__':
    # read info
    database = Database()
    max_id = database.get_max_id()
    current_paper = Paper()

    for line in open(path, 'r'):
        content = line[2:].strip(' \r\n')
        if line.startswith('#*'):
            current_paper.title = content
        elif line.startswith('#t'):
            current_paper.year = int(content)
        elif line.startswith('#c'):
            current_paper.publication = content
        elif line.startswith('#i'):
            current_paper.id = int(content[4:])
            if current_paper.id % 500 == 0:
                print current_paper.id
        elif line.startswith('#!'):
            current_paper.abstract = content
            #save
            try:
                if current_paper.id > max_id:
                    database.insertPaper(current_paper)
                    current_paper = Paper()
            except Exception, e:
                print e
                continue