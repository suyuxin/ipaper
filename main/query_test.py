__author__ = 'yxsu'

from database import Database


if __name__ == '__main__':
    query = "network"
    database = Database()
    doc_set = set()
    for term in query.split(' '):
        word_id = database.get_word_id(term)
        if len(doc_set) == 0:
            doc_set = database.get_doc_set_contain_word(word_id)
        else:
            doc_set.intersection_update(database.get_doc_set_contain_word(word_id))

    print database.get_sorted_doc_info_list(doc_set)



