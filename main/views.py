from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
from django.shortcuts import render
from database import Database

# Create your views here.
def index(request):
    if request.method == 'GET':
        if 'query' in request.GET:
            database = Database()
            doc_set = set()
            for term in request.GET['query'].split(' '):
                word_id = database.get_word_id(term)
                if len(doc_set) == 0:
                    doc_set = database.get_doc_set_contain_word(word_id)
                else:
                    doc_set.intersection_update(database.get_doc_set_contain_word(word_id))
            paper_list = database.get_sorted_paper_list(doc_set)
            return render_to_response('index.html', {'paper_list': paper_list})
    return render_to_response('index.html',{'paper_list': None})


def index_mobile(request):
    if request.method == 'GET' and 'query' in request.GET:
        database = Database()
        doc_set = set()
        try:
            for term in request.GET['query'].split(' '):
                word_id = database.get_word_id(term)
                if len(doc_set) == 0:
                    doc_set = database.get_doc_set_contain_word(word_id)
                else:
                    doc_set.intersection_update(database.get_doc_set_contain_word(word_id))
            if 'user_id' in request.GET:
                user_id = request.GET['user_id']
                user_prefer_doc_set = database.get_doc_list_prefered_by_user(user_id)
                if user_prefer_doc_set:
                    doc_set.intersection_update(user_prefer_doc_set)
            return HttpResponse(database.get_sorted_doc_info_list(doc_set))
        except Exception, e:
            return HttpResponse("No result")
    else:
        return HttpResponse("Input Format is invalid")

def mobile_abstract(request):
    if request.method == 'GET' and 'paper_id' in request.GET:
        database = Database()
        try:
            return HttpResponse(database.get_abstract(request.GET['paper_id']))
        except Exception, e:
            return HttpResponse("No result")

def mobile_user_model(request):
    if request.method == 'GET':
        if 'paper_id' in request.GET and 'user_id' in request.GET and 'pref_degree' in request.GET:
            database = Database()
            database.update_user_model_record(request.GET['user_id'], request.GET['paper_id'], request.GET['pref_degree'])
            return HttpResponse("Save sucessfully!")

    return HttpResponse("You should use GET method and provide three parameters: 'user_id', 'paper_id', 'pref_degree'")