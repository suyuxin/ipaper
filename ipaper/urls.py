from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('main.views',
    # Examples:
    # url(r'^$', 'ipaper.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'index'),
    url(r'^mobile/$', 'index_mobile'),
    url(r'^mobile/abstract/$', 'mobile_abstract'),
    url(r'^mobile/model/$', 'mobile_user_model'),

    url(r'^admin/', include(admin.site.urls)),
)